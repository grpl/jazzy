import { Component, OnInit, OnDestroy } from '@angular/core';
import { BracketService, BracketStore } from "./services/bracket.service";
import { Observable } from "rxjs";
import { distinctUntilChanged, tap, map } from "rxjs/operators";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  bracketsObservable$: Observable<BracketStore[]>;
  brackets: BracketStore[] = [];

  constructor(public bracketService: BracketService) {
  }

  ngOnInit() {
    this.bracketService.brackets$.subscribe((brackets) => {
      this.brackets = brackets;
    })
  }

  ngOnDestroy () {
    this.bracketService.brackets$.unsubscribe();
  }
}
