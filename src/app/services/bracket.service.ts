import { Injectable } from "@angular/core";
import { InputData, Bracket, Match, MatchPlayer, Matchup, Matchups, Players } from "../interfaces/bracket.interfaces";
import { SplitArray } from "../utils/split-array";
import { BehaviorSubject } from "rxjs";

export class BracketStore {
  public quarterfinals: Players;
  public semifinals: Matchup;
  public finals: Match;
  public finalist: MatchPlayer;

  constructor(private bracket: Match) {
    this.setFinalist(bracket);
    [this.finals, this.semifinals, this.quarterfinals] = this.deep([bracket], [[bracket]]);
    this.dataSerialization();
  }

  setFinalist(bracket: Match) {
    if (bracket.winner === null) {
      this.finalist = (!!bracket.upperPart.player) ? bracket.upperPart : bracket.lowerPart;

      return;
    }

    this.finalist = bracket[`${bracket.winner}Part`];
  }

  dataSerialization() {
    if (this.semifinals.length !== 2) {
      this.detectAndCopyPlayer(this.semifinals, this.finals);
    }

    if (this.quarterfinals && this.quarterfinals.some(matchup => matchup.length !== 2)) {
      this.detectAndCopyPlayer(this.quarterfinals, this.semifinals);
    }
  }

  detectAndCopyPlayer(scopeOfBeforeMatchup, scopeOfActualMetchup) {
    let levelBeforeMatchup = scopeOfBeforeMatchup[0];
    let actualMetchup = scopeOfActualMetchup[0];

    if (Array.isArray(levelBeforeMatchup)) {
      const scopeOfBeforeMatchupIndex = scopeOfBeforeMatchup.findIndex(matchup => matchup.length !== 2);
      actualMetchup = scopeOfActualMetchup[scopeOfBeforeMatchupIndex];
      levelBeforeMatchup = scopeOfBeforeMatchup[scopeOfBeforeMatchupIndex][0];
      scopeOfBeforeMatchup = scopeOfBeforeMatchup[scopeOfBeforeMatchupIndex];
    }

    let isUpper = [levelBeforeMatchup.upperPart.player, levelBeforeMatchup.lowerPart.player].some(name => name === actualMetchup.upperPart.player);
    let additionalMatchup = { ...JSON.parse(JSON.stringify(actualMetchup)), winner: null };

    if (isUpper) {
      delete additionalMatchup.upperPart;
      additionalMatchup.lowerPart.points = "";
      scopeOfBeforeMatchup.push(additionalMatchup);
    }
    else {
      delete additionalMatchup.lowerPart;
      additionalMatchup.upperPart.points = "";
      scopeOfBeforeMatchup.unshift(additionalMatchup);
    }
  }

  deep(node, layers) {
    let deepNodes = node.reduce(function(acc, currentValue){
      if (currentValue.hasOwnProperty('firstChild')) {
        acc.push(currentValue.firstChild);
      }

      if (currentValue.hasOwnProperty('secondChild')) {
        acc.push(currentValue.secondChild);
      }

      return acc;
    }, []);

    if (!!deepNodes.length) {
      deepNodes = (deepNodes.length >= 2 && layers.length === 2) ? SplitArray(deepNodes) : deepNodes;
      layers.push(deepNodes);

      return this.deep(deepNodes, layers);
    }

    return layers;
  }
}

@Injectable({
  providedIn: "root"
})
export class BracketService {
  public brackets$ = new BehaviorSubject<BracketStore[]>([]);

  setNewBracket(data) {
    const brackets = Object.keys(data.brackets).reduce(
      (acc, currentValue) => acc.concat(data.brackets[currentValue].bracketTops.map(bracket => new BracketStore(bracket))), []
    );

    this.brackets$.next(brackets);
  }
}
