import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { InlineSVGModule } from 'ng-inline-svg';

import { AppComponent } from './app.component';
import { BracketComponent } from "./components/bracket";
import { ParticipantComponent } from "./components/participant";
import { PlayersContainerComponent } from "./components/players-container";
import { SelectDataComponent } from "./components/select-data";

@NgModule({
  declarations: [
    AppComponent,
    SelectDataComponent,
    BracketComponent,
    ParticipantComponent,
    PlayersContainerComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    InlineSVGModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent],
  exports: [BracketComponent]
})
export class AppModule { }
