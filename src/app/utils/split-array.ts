export function SplitArray(arr, chunkSize = 2) {
  return arr.reduce(function(acc, v, index){
    if(index % chunkSize === 0) {
      acc.push([v])
    }
    else {
      acc[acc.length - 1].push(v);
    }

    return acc;
  }, []);
}
