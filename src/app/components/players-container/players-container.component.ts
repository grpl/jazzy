import { Component, Input, OnInit, HostBinding } from "@angular/core";
import { Match } from "../../interfaces/bracket.interfaces";

@Component({
  selector: "players-container",
  templateUrl: "players-container.component.html",
  styleUrls: ["players-container.component.scss"]
})
export class PlayersContainerComponent implements OnInit {
  @Input() players;
  @Input() type: string;

  @HostBinding('class') class;

  ngOnInit() {
    if (this.type === 'finals') {
      const isSingle = <Match>this.players[0].winner === null;
      this.class = [this.type, (isSingle) ? 'players-container--matchups-none' : ''].join(' ');

      return;
    }

    this.class = this.type;
  }
}
