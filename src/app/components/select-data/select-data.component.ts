import { Component, Input } from "@angular/core";
import { FormBuilder, FormGroup, FormControl } from "@angular/forms";
import { BracketService } from "../../services/bracket.service";
import { distinctUntilChanged, tap, map } from "rxjs/operators";
import { Bracket } from "../../interfaces/bracket.interfaces";
import basic1 from "./examples/basic/1.json";
import basic2 from "./examples/basic/2.json";
import basic3 from "./examples/basic/3.json";
import basic4 from "./examples/basic/4.json";
import doubleElimination1 from "./examples/double-elimination/1.json";
import doubleElimination2 from "./examples/double-elimination/2.json";
import doubleEliminationFinals1 from "./examples/double-elimination-finals/1.json";
import nonSymmetrical1 from "./examples/non-symmetrical/1.json";
import singleProceder1 from "./examples/single-proceder/1.json";
import singleProceder2 from "./examples/single-proceder/2.json";

type ExamplesData = { [key: string]: Bracket; };

const examplesData = {
  basic1: basic1,
  basic2: basic2,
  basic3: basic3,
  basic4: basic4,
  doubleElimination1: doubleElimination1,
  doubleElimination2: doubleElimination2,
  doubleEliminationFinals1: doubleEliminationFinals1,
  nonSymmetrical1: nonSymmetrical1,
  singleProceder1: singleProceder1,
  singleProceder2: singleProceder2
}

Object.freeze(examplesData);

@Component({
  selector: "select-data",
  templateUrl: "select-data.component.html",
  styleUrls: ["select-data.component.scss"]
})
export class SelectDataComponent {
  examplesData = examplesData;

  formGroup: FormGroup = this.fb.group({
    brackets: [Object.keys(this.examplesData)[0]]
  });

  constructor(private fb: FormBuilder, private bracketService: BracketService) {
    const brackets$ = this.formGroup.get("brackets").valueChanges;

    brackets$.pipe(distinctUntilChanged()).subscribe(bracketName => {
      if (!this.examplesData.hasOwnProperty(bracketName)) {
        return;
      }

      bracketService.setNewBracket(this.examplesData[bracketName]);
    });

    bracketService.setNewBracket(this.examplesData[Object.keys(this.examplesData)[0]]);
  }
}
