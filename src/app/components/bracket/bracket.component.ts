import { Component, Input } from "@angular/core";
import { BracketStore } from "../../services/bracket.service";

@Component({
  selector: "bracket",
  templateUrl: "bracket.component.html",
  styleUrls: ["bracket.component.scss"]
})
export class BracketComponent {
  @Input() bracket: BracketStore;
}
