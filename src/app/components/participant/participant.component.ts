import { Component, Input, HostBinding, OnInit } from "@angular/core";
import { MatchPlayer } from "../../interfaces/bracket.interfaces";

@Component({
  selector: "participant",
  templateUrl: "participant.component.html",
  styleUrls: ["participant.component.scss"]
})
export class ParticipantComponent implements OnInit {
  @Input() player: MatchPlayer;
  @Input() winner?: string = "";
  @Input() key?: string = "";
  @HostBinding("class") class;

  ngOnInit() {
    const winner = this.key === this.winner || this.winner === 'force';

    this.class = [
      "participant",
      (winner) ? "participant--winner" : "",
      (this.winner === null) ? "participant--without-winner" : ""
    ].join(" ");
  }
}
