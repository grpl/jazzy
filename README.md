## How to run

Hi!:) Below is a description of how to run the application.

1. Run in development mode: npm start (View in your browser on http://localhost:5006/)
2. Building a production application: npm run build
3. In the application you will check all cases. Use the brackets selector available on the site. You don't have to change the data manually.

---
